The purpose of this module is to integrate Simplenews and Organic Groups.

Installation steps:
1. Install the module
2. Configure the module (admin/config/services/simplenews/og)
2.a. Selecting which OG group content types have newsletters
2.b. Put in default settings for the newsletter category settings
3. Create newsletter categories for existing OG group nodes, if applicable
3.a. Go to the node edit form of existing OG group node
3.b. Click the "Newsletter Category" tab
3.c. Click the link to create the new category
